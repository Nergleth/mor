@extends('wrappers.default', ['module' => 'Contact'])

@section('subpage')
    <div class="page-contact">
        @include('components.CornerHeader.CornerHeader', ['title' => 'Kontakt'])
        <div class="container container--narrow page-contact__content">
            <div class="row">
                <div class="col-xs-12 col-lg-6">
                    <div data-aos="fade-right" class="page-contact__left">
                        <div class="cms-content">
                            <h5>KOPALNIA WAPIENIA "MORAWICA" S.A.</h5>
                            <p>ul. Górnicza 42<br/> 26-026 Morawica k/Kielc<br/> woj. świętokrzyskie, Polska<br/> NIP: 657-24-09-259</p>
                        </div>
                        <a href="/" class="white-btn">Zobacz na mapie</a>
                        <div class="cms-content">
                            <p>
                                ZAKŁAD CZYNNY - BIURO <br/>
                                poniedziałek - piątek 6.30 - 15.00 <br/>
                            </p>
                            <p>
                                ZAŁADUNKI: <br/>
                                Od poniedziałku 4.00 do soboty 14.00 (24h)
                            </p>
                        </div>
                        <div class="page-contact__email js-open-form">Napisz do nas</div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6">
                    <div data-aos="fade-left" class="page-contact__right">
                        <div class="section-header">Znajdź dział</div>
                        <div class="dep" id="js-dep">
                            <div class="dep__choose">Sekretariat</div>
                            <ul class="dep__list">
                                <li class="dep__list-elem js-dep-option" data-id="0">Opcja 1</li>
                                <li class="dep__list-elem js-dep-option" data-id="1">Opcja 2</li>
                                <li class="dep__list-elem js-dep-option" data-id="2">Opcja 3</li>
                                <li class="dep__list-elem js-dep-option" data-id="3">Opcja 4</li>
                            </ul>
                        </div>
                        <div class="dep-0 js-dep-content dep__content active">
                            <div class="person">
                                <div class="person__name">Bugajska Małgorzata</div>
                                <a href="tel:41 36 70 211" class="person__phone">tel: 41 36 70 211</a>
                                <a href="tel:41 36 70 299" class="person__phone">fax: 41 36 70 299</a>
                            </div>
                            <div class="person">
                                <div class="person__name">Koncewicz Bożena</div>
                                <a href="tel:41 36 70 282" class="person__phone">tel: 41 36 70 282</a>
                                <a href="mailto:b.koncewicz@kwmorawica.kielce.pl" class="person__phone">e-mail: b.koncewicz@kwmorawica.kielce.pl</a>
                            </div>
                        </div>
                        <div class="dep-1 js-dep-content dep__content">
                            <div class="person">
                                <div class="person__name">Lorem Ipsum</div>
                                <a href="tel:41 36 70 211" class="person__phone">tel: 41 36 70 211</a>
                                <a href="tel:41 36 70 299" class="person__phone">fax: 41 36 70 299</a>
                            </div>
                            <div class="person">
                                <div class="person__name">Tadeusz Kowalski</div>
                                <a href="tel:41 36 70 282" class="person__phone">tel: 41 36 70 282</a>
                                <a href="mailto:b.koncewicz@kwmorawica.kielce.pl" class="person__phone">e-mail: b.koncewicz@kwmorawica.kielce.pl</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('components.Download.Download', ['centered' => true])
    </div>
@endsection
