@extends('wrappers.default', ['module' => 'newslist'])

@section('subpage')
    <div class="page-newslist">
        @include('components.PageHeader.PageHeader', ['title' => 'Aktualności', 'hasTable' => false])
        <div class="container container--narrow">
            <div class="row row--padding-small">
                <div class="col-xs-12 col-sm-6 col-md-4 padding-small">
                    <a href="/" class="newsbox">
                        <img src="images/news.png" alt="Tytuł" class="newsbox__cover img-responsive">
                        <span class="newsbox__title">Tytuł pierwszej aktualności</span>
                        <span class="newsbox__intro">Jedynym sensownym rozwiązaniem zaistniałej sytuacji było odłączenie się od zakładu macierzystego i rozpoczęcie własnej działalności.</span>
                        <span class="newsbox__date">09.09.2020</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 padding-small">
                    <a href="/" class="newsbox">
                        <img src="images/news.png" alt="Tytuł" class="newsbox__cover img-responsive">
                        <span class="newsbox__title">WOW! Aktualność z dłuższym tytułem. To tak też się da!!</span>
                        <span class="newsbox__intro">Jedynym sensownym rozwiązaniem zaistniałej sytuacji było odłączenie się od zakładu macierzystego i rozpoczęcie własnej działalności.</span>
                        <span class="newsbox__date">09.09.2020</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 padding-small">
                    <a href="/" class="newsbox">
                        <img src="images/news.png" alt="Tytuł" class="newsbox__cover img-responsive">
                        <span class="newsbox__title">Tytuł pierwszej aktualności</span>
                        <span class="newsbox__intro">Jedynym sensownym rozwiązaniem zaistniałej sytuacji było odłączenie się od zakładu macierzystego i rozpoczęcie własnej działalności.</span>
                        <span class="newsbox__date">09.09.2020</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 padding-small">
                    <a href="/" class="newsbox">
                        <img src="images/news.png" alt="Tytuł" class="newsbox__cover img-responsive">
                        <span class="newsbox__title">WOW! Aktualność z dłuższym tytułem. To tak też się da!!</span>
                        <span class="newsbox__intro">Jedynym sensownym rozwiązaniem zaistniałej sytuacji było odłączenie się od zakładu macierzystego i rozpoczęcie własnej działalności.</span>
                        <span class="newsbox__date">09.09.2020</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 padding-small">
                    <a href="/" class="newsbox">
                        <img src="images/news.png" alt="Tytuł" class="newsbox__cover img-responsive">
                        <span class="newsbox__title">Tytuł pierwszej aktualności</span>
                        <span class="newsbox__intro">Jedynym sensownym rozwiązaniem zaistniałej sytuacji było odłączenie się od zakładu macierzystego i rozpoczęcie własnej działalności.</span>
                        <span class="newsbox__date">09.09.2020</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 padding-small">
                    <a href="/" class="newsbox">
                        <img src="images/news.png" alt="Tytuł" class="newsbox__cover img-responsive">
                        <span class="newsbox__title">WOW! Aktualność z dłuższym tytułem. To tak też się da!!</span>
                        <span class="newsbox__intro">Jedynym sensownym rozwiązaniem zaistniałej sytuacji było odłączenie się od zakładu macierzystego i rozpoczęcie własnej działalności.</span>
                        <span class="newsbox__date">09.09.2020</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
