@extends('wrappers.default', ['module' => 'product'])

@section('subpage')
    <div class="page-product">
        @include('components.PageHeader.PageHeader', ['title' => 'Kruszywa drogowe i budowlane', 'hasTable' => true])
        <div class="container">
            <div class="row row--lg-justify-content-between product__content-box">
                <div data-aos="fade-left" class="col-xs-12 col-md-6">
                    <div class="cms-content">
                        <h5>Kruszywa mają zastosowanie w:</h5>
                        <ul>
                            <li>produkcji betonów</li>
                            <li>stabilizacji gruntów</li>
                            <li>na podbudowę dróg</li>
                            <li>utwardzania placów</li>
                        </ul>
                    </div>
                </div>
                <div data-aos="fade-right" class="col-xs-12 col-md-6 col-lg-5">
                    <img alt="Kruszywa" class="img-responsive product__image" src="/images/kruszywa1.png"/>
                </div>
            </div>
            <div class="row row--lg-justify-content-between row--md-row-reverse product__content-box">
                <div data-aos="fade-up" class="col-xs-12 col-md-6">
                    <div class="cms-content">
                        <h5>Posiadamy listy rekomendacyjne od znaczących w kraju przedsiębiorstw budowlanych polecające nasze kruszywa do szeregu prac takich jak:</h5>
                        <p>Prace wykonywane przy budowie trasy S8 w Warszawie przez firmy: Strabag Sp. z o.o.; Bilfinger Berger Polska S.A.; Budimex Dromex S.A. prace wykonywane przez firmę "Hermann Kirchner Polska Sp. z o.o." przy budowie autostrady A2, odcinek Emilia - Stryków prace realizowane przez firmę Bunte Polska Sp. z o.o. przy budowie autostrady A2 betony produkowane przez "Jotrex Bis" Piaseczno dla osiedli mieszkaniowych w Piasecznie i Józefosławie, biurowiec firmy "Zepter" w Warszawie betony produkowane przez CEMEX Polska Sp. z o.o. prace wykonywane przez "J&P-AVAX S.A.." przy budowie autostrady A4 Radymno - Korczowa budowy realizowane przez Skanska S.A. na terenie całego kraju prace firmy "Budosort" na budowach hipermarketu KCC I -Geant Casino Łódź, magazynów wysokiego składowania w Strykowie czy magazynów Prologi w Teresinie</p>
                        <p><strong>Lorem ipsum bold text</strong></p>
                        <p>Prace wykonywane przy budowie trasy S8 w Warszawie przez firmy: Strabag Sp. z o.o.; Bilfinger Berger Polska S.A.; Budimex Dromex S.A. prace wykonywane przez firmę "Hermann Kirchner Polska Sp. z o.o." przy budowie autostrady A2, odcinek Emilia - Stryków prace realizowane przez firmę Bunte Polska Sp. z o.o. przy budowie autostrady A2 betony produkowane przez "Jotrex Bis" Piaseczno dla osiedli mieszkaniowych w Piasecznie i Józefosławie, biurowiec firmy "Zepter" w Warszawie betony produkowane przez CEMEX Polska Sp. z o.o. prace wykonywane przez "J&P-AVAX S.A.." przy budowie autostrady A4 Radymno - Korczowa budowy realizowane przez Skanska S.A. na terenie całego kraju prace firmy "Budosort" na budowach hipermarketu KCC I -Geant Casino Łódź, magazynów wysokiego składowania w Strykowie czy magazynów Prologi w Teresinie</p>
                    </div>
                </div>
                <div data-aos="fade-down" class="col-xs-12 col-md-6 col-lg-5"><img alt="Kruszywa" class="img-responsive product__image" src="/images/kruszywa1.png"/></div>
            </div>
            <div data-aos="fade-left" class="row row--lg-justify-content-between row--lg-align-center product__content-box">
                <div class="col-xs-12 col-md-6">
                    <div class="cms-content">
                        <h5>Kruszywa mają zastosowanie w:</h5>
                        <ul>
                            <li>produkcji betonów</li>
                            <li>stabilizacji gruntów</li>
                            <li>na podbudowę dróg</li>
                            <li>utwardzania placów</li>
                        </ul>
                    </div>
                </div>
                <div data-aos="fade-left" class="col-xs-12 col-md-6 col-lg-5">
                    <img alt="Kruszywa" class="img-responsive product__image" src="/images/product2.png"/>
                </div>
            </div>
        </div>
        @include('components.Download.Download')
        <div class="yellow-info">
                <div class="cms-content text-xs-center">
                    <h5><strong>Utrzymanie optymalnego zakresu odczynu (pH) gleb w gospodarstwie jest podstawowym<br /> warunkiem opłacalności produkcji roślinnej.</strong></h5>
                    <p>Niespełnienie tego warunku oznacza znaczną utratę plonu. Poprzez systematyczne wapnowanie można osiągalność wzrost plonu <strong>nawet o 25%</strong></p>
                </div>
        </div>
        @include('components.Gallery.Gallery')
    </div>
@endsection
