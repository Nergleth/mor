@extends('wrappers.default', ['module' => 'QualityPolicy'])

@section('subpage')
    <div class="page-quality-policy">
        @include('components.CornerHeader.CornerHeader', ['title' => 'Polityka jakości'])
        <div class="container container--narrow">
            <div data-aos="fade-up" class="cms-content">
                <p>Kopalnia Wapienia „Morawica” S.A. jest podmiotem gospodarczym (organizacją), która ustanowiła i utrzymuje ZINTEGROWNY SYSTEM ZARZĄDZANIA. System ten jest gwarancją realizacji polityki jakości i celów jakości. Wdrożony System został ustanowiony w sposób, który gwarantuje jego ciągłość, mimo możliwych zmian polityki jakości i celów jakości z niej wynikających. System podlega okresowym przeglądom i auditom wewnętrznym oraz zewnętrznym, służącym nadzorowaniu oraz jego stałemu doskonaleniu, ukierunkowanemu na zwiększenie zadowolenia klientów.</p>
                <p>Zintegrowanym Systemem Zarządzania objęte są wszystkie jednostki organizacyjne firmy. Obowiązuje on w zakresie eksploatacji i produkcji:</p>
                <ul>
                    <li>kamienia wapiennego przemysłowego;</li>
                    <li>kruszyw mineralnych łamanych;</li>
                    <li>wyrobów marmurowych;</li>
                    <li>nawozów węglanowych.</li>
                </ul>
                <p>System został ustanowiony oraz funkcjonuje w oparciu o:</p>
                <ul>
                    <li>Księgę Jakości;</li>
                    <li>Księgę Zakładowej Kontroli Produkcji;</li>
                    <li>procedury i instrukcje;</li>
                    <li>wewnętrzne akty normatywne (regulaminy, zarządzenia, itp.), dokumenty zewnętrzne (normy branżowe i państwowe, przepisy Urzędu Dozoru Technicznego, katalogi, itp.);</li>
                    <li>dokumentację techniczną (warsztatową).</li>
                </ul>
                <p>Cele dotyczące jakości:</p>
                <ul>
                    <li>zapewnienie rozwoju firmy poprzez poprawę efektywności ekonomicznej, szkolenie personelu, poprawę gospodarki materiałowej, utrzymanie gotowości eksploatacyjnej maszyn i urządzeń. Wszystkie te warunki muszą być spełnione przy zapewnieniu bezpieczeństwa pracy pracowników oraz ochrony środowiska;</li>
                    <li>zapewnienie, iż wymagania odpowiednich norm, przepisów dotyczących wyrobów i świadczonych usług są spełnione, a fakt spełnienia jest odpowiednio zapisany oraz udokumentowany;</li>
                    <li>zwiększenie zaufania i zadowolenia klientów poprzez dostarczanie wyrobów i usług w terminie i bez wad;</li>
                    <li>zapewnienie powtarzalności naszych wyrobów, a więc ich zgodności z wymaganiami właściwych norm;</li>
                    <li>zapewnienie powtarzalności kontraktów handlowych oraz odnawiania umów. Nasi partnerzy mają pewność otrzymywania kolejnych dostaw zgodnych z wymaganiami.</li>
                    <li>wyniki badań laboratoryjnych w zakresie produkcji kruszyw zgodne z wymaganiami norm uzyskane przez własne laboratorium, wyniki badań uzyskane przez laboratoria zewnętrzne oraz atesty dotyczące wapna nawozowego węglanowego wystawione przez laboratorium zewnętrzne świadczą o powtarzalności wyrobów. Cechy uznawane za świadczące o powtarzalności są ściśle określone w normach. Zapisy dotyczące wyników badań wyrobów oraz ich dopuszczenia do obrotu, tj. Świadectwa Jakości oraz Deklaracje Właściwości Użytkowych są udostępnianie klientom. Powtarzalność wyrobów potwierdzają również wyniki badań przeprowadzanych przez naszych odbiorców strategicznych we własnych laboratoriach.</li>
                </ul>
                <h5>ZINTEGROWANY SYSTEM ZARZĄDZANIA funkcjonuje w oparciu o dwa filary:</h5>
                <h6>System zarządzania jakością</h6>
                <p>SYSTEM ZARZĄDZANIA JAKOŚCIĄ System ten funkcjonuje od 1998 r. Spełnia on wymagania normy PN-EN ISO 9001:2015 „Systemy Zarządzania Jakością. Wymagania”. System uprzednio uzyskał certyfikację na zgodność z normami:</p>
                <ul>
                    <li>PN-EN 9001:2008</li>
                    <li>PN-EN 9001:2000</li>
                </ul>
                <h6>System zakładowej kontroli produkcji</h6>
                <p>System ten funkcjonuje od 2010 r. Wprowadzony został w oparciu o Rozporządzenie PE i Rady UE nr 305/2011 z dnia 09.03.2011, ustanawiające zharmonizowane warunki wprowadzenia do obrotu wyrobów budowlanych i uchylające dyrektywę Rady 89/106/EWG. System uzyskał certyfikację Instytutu Mechanizacji Budownictwa i Górnictwa Skalnego w ramach systemu 2+ na zgodność z normami:</p>
                <ul>
                    <li>PN-EN 13043:2002+AC:2004 „Kruszywa do mieszanek bitumicznych i powierzchniowych utrwaleń stosowanych na drogach i lotniskach i innych powierzchniach przeznaczonych do ruchu”. </li>
                    <li>PN-EN 13242:2002+A1:2007 „Kruszywa do niezwiązanych i związanych hydraulicznie materiałów stosowanych w obiektach budowlanych i budownictwie drogowym”.</li>
                </ul>
            </div>
            <div data-aos="fade-down" class="certificates">
                <div class="certificate">
                    <div class="certificate__image-wrapper">
                        <img src="/images/certificate.png" alt="" class="certificate__image">
                    </div>
                    <div class="certificate__title">Certyfikat ISO</div>
                </div>
                <div class="certificate">
                    <div class="certificate__image-wrapper">
                        <img src="/images/certificate.png" alt="" class="certificate__image">
                    </div>
                    <div class="certificate__title">Certyfikat ISO EN</div>
                </div>
                <div class="certificate">
                    <div class="certificate__image-wrapper">
                        <img src="/images/certificate.png" alt="" class="certificate__image">
                    </div>
                    <div class="certificate__title">Certyfikat zgodności</div>
                </div>
            </div>
            <div data-aos="fade-up" class="data-table">
                <div class="data-table__title">Tabela prezentująca dane i statystyki</div>
                <div class="data-table__row">
                    <div class="data-table__desc">Wiersz z opisem</div>
                    <div class="data-table__value">Wartość</div>
                </div>
                <div class="data-table__row data-table__row--transparent">
                    <div class="data-table__desc">Wiersz z opisem</div>
                    <div class="data-table__value">Wartość</div>
                </div>
                <div class="data-table__row">
                    <div class="data-table__desc">Wiersz z opisem</div>
                    <div class="data-table__value">Wartość</div>
                </div>
                <div class="data-table__row data-table__row--transparent">
                    <div class="data-table__desc">Wiersz z opisem</div>
                    <div class="data-table__value">Wartość</div>
                </div>
            </div>
        </div>
    </div>
@endsection
