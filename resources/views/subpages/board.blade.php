@extends('wrappers.default', ['module' => 'board'])

@section('subpage')
    <div class="page-board">
        @include('components.CornerHeader.CornerHeader', ['title' => 'Zarząd firmy'])
        <div class="container container--thin">
            <div data-aos="fade-up" class="board-person board-person--main">
                <div class="board-person__name">Dąbek Józef</div>
                <div class="board-person__position">Prezes Zarządu</div>
            </div>
            <div data-aos="fade-up" class="board-person">
                <div class="board-person__name">Artur Krzak</div>
                <div class="board-person__position">Prokurent</div>
            </div>
            <div data-aos="fade-up" class="board-person">
                <div class="board-person__name">Konrad Głuc</div>
                <div class="board-person__position">Prokurent</div>
            </div>
        </div>
    </div>
@endsection
