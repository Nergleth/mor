@extends('wrappers.default', ['module' => 'history'])

@section('subpage')
    <div class="page-history">
        @include('components.CornerHeader.CornerHeader', ['title' => 'Historia firmy'])
        <div class="container container--thin">
            <div class="history-step">
                <div class="history-step__year" data-aos="fade-up">1964</div>
                <div class="history-step__desc" data-aos="fade-left">Powstał nieistniejący już Zakład Produkcji Kruszyw Łamanych</div>
            </div>
            <div class="history-step">
                <div class="history-step__year" data-aos="fade-up">Do 1964</div>
                <div class="history-step__desc" data-aos="fade-left">Powstał nieistniejący już Zakład Produkcji Kruszyw Łamanych</div>
            </div>
            <div class="history-step">
                <div class="history-step__year" data-aos="fade-up">1964</div>
                <div class="history-step__desc" data-aos="fade-left">Powstał nieistniejący już Zakład Produkcji Kruszyw Łamanych</div>
            </div>
            <div class="history-step">
                <div class="history-step__year" data-aos="fade-up">1964</div>
                <div class="history-step__desc" data-aos="fade-left">Dostarczanie kruszyw budowlanych do „fabryk domów” – w latach 70. i 80. budowano z nich osiedla mieszkaniowe m.in. w Lublinie, Warszawie, Kielcach, Łodzi i Krakowie</div>
            </div>
        </div>
    </div>
@endsection
