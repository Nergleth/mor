@extends('wrappers.default', ['module' => 'awards'])

@section('subpage')
    <div class="page-awards">
        @include('components.CornerHeader.CornerHeader', ['title' => 'Nagrody i wyróżnienia'])
        <div class="container container--narrow">
            <div class="cms-content">
                <p>Kopalnia Wapienia „Morawica” S.A. jest podmiotem gospodarczym (organizacją), która ustanowiła i utrzymuje ZINTEGROWNY SYSTEM ZARZĄDZANIA. System ten jest gwarancją realizacji polityki jakości i celów jakości. Wdrożony System został ustanowiony w sposób, który gwarantuje jego ciągłość, mimo możliwych zmian polityki jakości i celów jakości z niej wynikających. System podlega okresowym przeglądom i auditom wewnętrznym oraz zewnętrznym, służącym nadzorowaniu oraz jego stałemu doskonaleniu, ukierunkowanemu na zwiększenie zadowolenia klientów.</p>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="award">
                            <div class="award__image-wrapper">
                                <img src="/images/award.png" alt="" class="award__image">
                            </div>
                            <h4 class="award__title">Za największe inwestycje Kielecczyzny</h4>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="award">
                            <div class="award__image-wrapper">
                                <img src="/images/award.png" alt="" class="award__image">
                            </div>
                            <h4 class="award__title">Za największe inwestycje Kielecczyzny</h4>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="award">
                            <div class="award__image-wrapper">
                                <img src="/images/award.png" alt="" class="award__image">
                            </div>
                            <h4 class="award__title">Nasze Dobre Świętokrzyskie – Znak Jakości „Echa Dnia” i „Najpopularniejszy Produkt Świętokrzyski”</h4>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="award">
                            <div class="award__image-wrapper">
                                <img src="/images/award.png" alt="" class="award__image">
                            </div>
                            <h4 class="award__title">Euro Partner</h4>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="award">
                            <div class="award__image-wrapper">
                                <img src="/images/award.png" alt="" class="award__image">
                            </div>
                            <h4 class="award__title">Za największe inwestycje Kielecczyzny</h4>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="award">
                            <div class="award__image-wrapper">
                                <img src="/images/award.png" alt="" class="award__image">
                            </div>
                            <h4 class="award__title">Za największe inwestycje Kielecczyzny</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
