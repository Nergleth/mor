@extends('wrappers.default', ['module' => 'Subsidies'])

@section('subpage')
    <div class="page-subsidies">
        @include('components.CornerHeader.CornerHeader', ['title' => 'Dotacje'])
        <div class="container container--narrow page-contact__content">
            <div class="row">
                <div class="col-xs-12 col-md-3 col-lg-4" data-sticky-container>
                    <div class="page-subsidies__left">
                        <div class="js-sticky" data-margin-top="190">
                            <div class="cms-content"><h5>Podkategorie</h5></div>
                            <ul class="subsidies-menu">
                                <li class="subsidies-menu__item"><a class="subsidies-menu__link" href="/subsidies">Przesiewacz</a></li>
                                <li class="subsidies-menu__item"><a class="subsidies-menu__link" href="/subsidies">Fotowoltaika</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 col-lg-8">
                    <div class="page-subsidies__right">
                        <div class="cms-content">
                            <h5>Nowe produkty w ofercie handlowej Kopalni Wapienia "Morawica" S.A.</h5>
                            <p>Informujemy, że w ramach realizacji projektu: "Zakup mobilnego dwupokładowego przesiewacza celem wdrożenia w Kopalnia Wapienia "Morawica" S.A. innowacyjności produktowej oraz procesowej z zakresu produkcji kruszyw", został zakupiony mobilny dwupokładowy przesiewacz - maszyna sortująca z koszem zasypowym i dwoma pokładami sit oraz napędem gąsiennicowym.</p>
                            <img src="/images/subsidies1.png" alt="">
                            <p>Zakup tego przesiewacza umożliwił wprowadzenie:</p>
                            <p>1. innowacji procesowej - co pozwala na eliminację frakcji drobnych (do dalszego wykorzystania w produkcji ekologicz
                                nego nawozu wapniowego), nowy proces frakcjonowania
                                - produkcja nowych, dotychczas nie produkowanych asortymentów ze
                                szczególnym uwzględnieniem poszukiwanych przez elektrownie i ciepłownie kamieniem wapiennym do instalacji odsiarczania spalin, </p>
                            <p>2. innowacji produktowej - która w sposób znaczący poprawia jakość produkowanych asortymentów,
                                ustanowi nową jakość kamienia wapiennego do odsiarczania spalin, separacja kamieni ozdobnych, wykorzystanie drobnych frakcji.</p>
                            <img src="/images/subsidies2.png" alt="">
                            <p>Inwestycja wpisuje się w sposób jednoznaczny w proces zmniejszenia hałasu i
                                zanieczyszczenia środowiska poprzez zastosowanie napędu hybrydowego oraz zagospodarowanie
                                w całości urobku z wyrobiska.
                             </p>
                            <p>Dla obsługi przesiewacza mobilnego stworzono 3 nowe miejsca pracy do sterowania maszyną za pomocą paneli cyfrowych.</p>
                            <p>Oferujemy nowe asortymenty w atrakcyjnych cenach.</p>
                            <p>Zapraszamy do współpracy zainteresowane firmy i klientów indywidualnych.</p>
                        </div>
                        @for ($i = 1; $i < 4; $i++)
                            @include('components.Subsidy.Subsidy', ['title' => 'Dotyczy rozstrzygnięcia zapytania ofertowego nr 1 z dnia 23-02-2018 r.', 'content' => '', 'files' => []])
                        @endfor
                    </div>
                </div>
            </div>
        </div>
        @include('components.Gallery.Gallery')
    </div>
@endsection
