@extends('wrappers.default', ['module' => 'homepage'])

@section('subpage')
    <div class="page-homepage">
        @include('components.Slider.Slider')
        <div class="eu eu--borderless" data-aos="fade-up">
            <img class="img-responsive eu__image" src="/images/rpo.png" alt="RPO" />
        </div>
        <div class="products">
            <div class="section-header section-header--center products__header" data-aos="fade-up">Nasze produkty</div>
            <div class="container">
                <div class="row">
                    @for ($i = 1; $i < 5; $i++)
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="products__product" data-aos="fade-up">
                                <div class="products__product-content">
                                    <img class="img-responsive products__product-sample" src="/images/pr{{$i}}.png" alt="Product"/>
                                    <div class="products__product-name">Kruszywa drogowe i budowlane</div>
                                </div>
                                <a href="/" class="products__product-link">
                                    <img class="img-responsive products__product-link-image" src="/images/icons/right-arrow.png" alt="Product"/>
                                </a>
                                <img class="img-responsive products__product-image" src="/images/product.jpg" alt="Product"/>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
        <div class="about">
            <div class="container">
                <div class="row row--xs-col-reverse row--md-row">
                    <div class="col-xs-12 col-md-6">
                        <div class="section-header" data-aos="fade-up">Kopalnia wapienia<br/> Morawica</div>
                        <div class="about__content cms-content" data-aos="fade-up">
                            <p>Przez ponad 40 lat funkcjonowania na rynku zdobyła ogromne doświadczenie i zaufanie klientów. Kopalnia Wapienia „Morawica”
                                SA to gwarancja jakości i dobra świętokrzyska marka.</p>
                            <p>Ma ona bardzo duże znaczenie nie tylko dla regionu, ale dla całego kraju, ponieważ jest jedną z największych
                                kopalni odkrywkowych w Polsce. Roczne wydobycie wynosi około 3 milionów ton kamienia,
                                który jest pozyskiwany z własnych złóż.</p>
                        </div>
                        <a href="http://google.pl" class="yellow-btn"><span>Nasza polityka jakości</span></a>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="about__right">
                            <img class="about__image" src="/images/about.jpg" alt="RPO" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="support">
            <div class="container">
                <div class="section-header">Wspieramy przemysł</div>
            </div>
            <div class="row no-margin">
                <div class="col-xs-12 col-lg-6 no-padding">
                    <div class="support__box">
                        <div class="row no-margin row--xs-col-reverse row--md-row">
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div class="support__box-content" data-aos="slide-up">
                                    <div class="support__box-title" data-aos="fade-up">Budownictwo i drogownictwo</div>
                                    <div class="support__box-desc" data-aos="fade-in">Sed sed feugiat arcu. Duis vel hendrerit lacus.
                                        Phasellus ultricies pulvinar odio, eu commodo ligula. Aliquam erat volutpat.
                                        Suspendisse ac rutrum lorem. Pellentesque imperdiet sed libero eget tincidunt.
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 no-padding">
                                <img data-aos="fade-right" class="img-responsive support__image" src="/images/sup1.png" alt="Support"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <div class="support__box">
                        <div class="row no-margin row--xs-col-reverse row--md-row">
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div class="support__box-content" data-aos="slide-up">
                                    <div class="support__box-title">Budownictwo i drogownictwo</div>
                                    <div class="support__box-desc">Sed sed feugiat arcu. Duis vel hendrerit lacus.
                                        Phasellus ultricies pulvinar odio, eu commodo ligula. Aliquam erat volutpat.
                                        Suspendisse ac rutrum lorem. Pellentesque imperdiet sed libero eget tincidunt.
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 no-padding">
                                <img data-aos="fade-right" class="img-responsive support__image" src="/images/sup2.png" alt="Support"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <div class="support__box">
                        <div class="row no-margin row--xs-col-reverse row--md-row-reverse">
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div class="support__box-content support__box-content--reverse">
                                    <div class="support__box-title">Budownictwo i drogownictwo</div>
                                    <div class="support__box-desc">Sed sed feugiat arcu. Duis vel hendrerit lacus.
                                        Phasellus ultricies pulvinar odio, eu commodo ligula. Aliquam erat volutpat.
                                        Suspendisse ac rutrum lorem. Pellentesque imperdiet sed libero eget tincidunt.
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 no-padding">
                                <img data-aos="fade-right" class="img-responsive support__image" src="/images/sup3.png" alt="Support"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <div class="support__box">
                        <div class="row no-margin row--xs-col-reverse row--md-row-reverse">
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div class="support__box-content support__box-content--reverse">
                                    <div class="support__box-title">Budownictwo i drogownictwo</div>
                                    <div class="support__box-desc">Sed sed feugiat arcu. Duis vel hendrerit lacus.
                                        Phasellus ultricies pulvinar odio, eu commodo ligula. Aliquam erat volutpat.
                                        Suspendisse ac rutrum lorem. Pellentesque imperdiet sed libero eget tincidunt.
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 no-padding">
                                <img data-aos="fade-right" class="img-responsive support__image" src="/images/sup4.png" alt="Support"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <div class="support__box">
                        <div class="row no-margin row--xs-col-reverse row--md-row">
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div class="support__box-content" data-aos="slide-up">
                                    <div class="support__box-title">Budownictwo i drogownictwo</div>
                                    <div class="support__box-desc">Sed sed feugiat arcu. Duis vel hendrerit lacus.
                                        Phasellus ultricies pulvinar odio, eu commodo ligula. Aliquam erat volutpat.
                                        Suspendisse ac rutrum lorem. Pellentesque imperdiet sed libero eget tincidunt.
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 no-padding">
                                <img data-aos="fade-right" class="img-responsive support__image" src="/images/sup5.png" alt="Support"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <div class="support__box">
                        <div class="row no-margin row--xs-col-reverse row--md-row">
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div class="support__box-content" data-aos="slide-up">
                                    <div class="support__box-title">Budownictwo i drogownictwo</div>
                                    <div class="support__box-desc">Sed sed feugiat arcu. Duis vel hendrerit lacus.
                                        Phasellus ultricies pulvinar odio, eu commodo ligula. Aliquam erat volutpat.
                                        Suspendisse ac rutrum lorem. Pellentesque imperdiet sed libero eget tincidunt.
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 no-padding">
                                <img data-aos="fade-right" class="img-responsive support__image" src="/images/sup6.png" alt="Support"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="experience">
            <div class="section-header section-header--center experience__header"data-aos="fade-in">Ogromne doświadczenie</div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="experience__box">
                            <img data-aos="fade-right" class="img-responsive experience__image" src="/images/exp1.jpg" alt="Doświadczenie"/>
                            <div class="experience__title" data-aos="fade-left">Jesteśmy ponad 55 lat<br/> na rynku wydobywczym</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="experience__box">
                            <img data-aos="fade-right" class="img-responsive experience__image" src="/images/exp2.jpg" alt="Doświadczenie"/>
                            <div class="experience__title" data-aos="fade-left">Pozyskujemy 3 miliony<br/> ton kamienia rocznie</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="experience__box">
                            <img data-aos="fade-right" class="img-responsive experience__image" src="/images/exp3.jpg" alt="Doświadczenie"/>
                            <div class="experience__title" data-aos="fade-left">Stale udoskonalamy<br/> procesy produkcyjne</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="join">
            <div class="container container--full">
                <div class="row">
                    <div class="col-xs-12 col-md-6 no-padding">
                        <img data-aos="fade-left" class="img-responsive join__image" src="/images/join.jpg" alt="Product"/>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="join__right" data-aos="fade-up">
                            <div class="section-header">Dołącz do nas<br/> i rozwijaj się z nami!</div>
                            <div class="about__content cms-content">
                                <p>Marzymy o przyszłości bardziej przyjaznej dla środowiska i korzystniejszej dla wszystkich. Aby osiągnąć ten cel, potrzebujemy najlepszych ludzi, którzy z
                                    pasją
                                    potraktują naszą misję i będą dumni z tego, kim jesteśmy..</p>
                            </div>
                            <a href="http://google.pl" class="yellow-btn"><span>Oferty pracy</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
