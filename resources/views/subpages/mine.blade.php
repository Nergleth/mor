@extends('wrappers.default', ['module' => 'mine'])

@section('subpage')
    <div class="page-mine">
        @include('components.PageHeader.PageHeader', ['title' => 'Kopalnia Morawica', 'hasTable' => false, 'narrow' => true])
        <div class="container container--narrow">
            <div class="row">
                <div data-aos="fade-up" class="col-xs-12">
                    <div class="cms-content">
                        <h5>Eksploatacja kamienia wapiennego w Kopalni Wapienia „Morawica” SA ma w swoją bogatą i długą historię. Kopalnia funkcjonuje od ponad 55 lat, chociaż kamień wapienny wydobywa się na tych terenach od ok.160 lat.</h5>
                        <p>Infrastruktura: dwa stacjonarne zakłady przeróbki mechanicznej surowca, zakład obróbki marmurów (parapety, posadzki itp.), bocznica kolejowa, trzy-poziomowe wyrobisko główne, wyrobisko bloczne, zaplecze techniczne.</p>
                    </div>
                </div>
                <div data-aos="fade-up" class="col-xs-12">
                    <div class="row page-mine__contents">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="cms-content">
                                <h5>ok. 220 ha</h5>
                                <p>Powierzchnia przedsiębiorstwa</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="cms-content">
                                <h5>100 ha</h5>
                                <p>Powierzchnia wyrobiska<br /> eksploatacyjnego (docelowo 125 ha)</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="cms-content">
                                <h5>3</h5>
                                <p>Ilość czynnych poziomów eksploatacyjnych</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="cms-content">
                                <h5>3 000 000 ton</h5>
                                <p>Powierzchnia przedsiębiorstwa</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="cms-content">
                                <h5>100 ha</h5>
                                <p>Powierzchnia wyrobiska<br /> eksploatacyjnego (docelowo 125 ha)</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="cms-content">
                                <h5>3</h5>
                                <p>Ilość czynnych poziomów eksploatacyjnych</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" class="col-xs-12">
                    <div class="cms-content page-mine__second-content">
                        <iframe width="100%" src="https://www.youtube.com/embed/WXv31OmnKqQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <p>Przed II wojną światową ze zlokalizowanego w Woli Morawickiej złoża Morawica I wydobywano bloki marmurowe, które następnie obrabiano, produkując płyty, płytki, stopnie, parapety itp. Produkcja masowa rozpoczęła się, gdy zaczęto wydobywać kamień i wypalać wapień w piecach kręgowych. Natomiast prowadzona obecnie działalność produkcji kruszyw zaczęła się na tych terenach w 1964 roku, kiedy powstał nieistniejący już Zakład Produkcji Kruszyw Łamanych. W 1966 roku rozpoczął produkcję Zakład Produkcji Kruszyw „Morawica II”. Dziesięć lat później ruszyła produkcja w Zakładzie „Morawica III”, który był inwestycją rządową i miał być największym zakładem produkującym kruszywa w Europie. Skończyło się jednak na I etapie, gdyż zabrakło środków i nie dokończono II etapu części produkcyjnej. Ale wraz z I etapem wybudowano całe zaplecze: linię kolejową, bocznice, drogi, uzbrojenie itp. Do 1990 roku dostarczaliśmy kruszywa budowlane do „fabryk domów” – w latach 70. i 80. budowano z nich osiedla mieszkaniowe m.in. w Lublinie, Warszawie, Kielcach, Łodzi i Krakowie.</p>
                    </div>
                </div>
            </div>
            <div class="row page-mine__two-cols">
                <div class="col-xs-12 col-sm-6" data-aos="fade-left">
                    <img class="img-responsive" src="/images/mine.png" alt="Kopalnia">
                </div>
                <div class="col-xs-12 col-sm-6" data-aos="fade-right">
                    <div class="cms-content">
                        <p>W marcu 1990 roku dyrekcja przedsiębiorstwa macierzystego postanowiła zlikwidować zakłady przeróbcze w Morawicy. Ta decyzja wywołała zdecydowany sprzeciw pracowników Kopalni w Morawicy. Likwidacja zakładu przeróbczego wiązała się ze zwolnieniami grupowymi około 400 jego pracowników.</p>
                    </div>
                </div>
            </div>
            <div class="row row--md-row-reverse page-mine__two-cols">
                <div class="col-xs-12 col-sm-6">
                    <img class="img-responsive" src="/images/mine.png" alt="Kopalnia">
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="cms-content">
                        <p>Jedynym sensownym rozwiązaniem zaistniałej sytuacji było odłączenie się od zakładu macierzystego i rozpoczęcie własnej działalności. W połowie 1990 roku został złożony wniosek o odłączenie zakładu przeróbczego w Morawicy od centrali przedsiębiorstwa. Wniosek został poparty analizą ekonomiczno- finansową, która stanowiła że w perspektywie kilku lat nowo powstały podmiot gospodarczy będzie w stanie prowadzić własny byt ekonomiczny, a jego działalność będzie rentowna. Takie decyzje miały zagwarantować utrzymanie wszystkich miejsc pracy w przyszłości. Kopalnia jako samodzielne przedsiębiorstwo prowadzi działalność od 1991 roku. W 1996 uruchomiony został Zakład Obróbki Marmuru To oddzielna, bardzo prestiżowa produkcja. W 2001 roku Kopalnia Wapienia "Morawica" przeszła proces prywatyzacji i od tego czasu stanowi spółkę akcyjną. Ma ona bardzo duże znaczenie nie tylko dla regionu, ale dla całego kraju, ponieważ jest jedną z największych kopalni odkrywkowych w Polsce. Roczne wydobycie wynosi około 3 milionów ton kamienia, który jest pozyskiwany z własnych złóż. Kopalnia posiada swoją bocznicę kolejową o dużych możliwościach załadunku oraz własne laboratorium.</p>
                    </div>
                </div>
            </div>
            <div class="row page-mine__two-cols" data-aos="fade-up">
                <div class="col-xs-12 col-sm-6">
                    <img class="img-responsive" src="/images/mine.png" alt="Kopalnia">
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="cms-content">
                        <p>Kopalnia Wapienia „Morawica” SA jest dostawcą dla wielu dziedzin gospodarki: budownictwa i drogownictwa, hutnictwa, energetyki, przemysłu cukrowniczego, rolnictwa. Firma produkuje kruszywa drogowe i budowlane, których jakość, potwierdzona jest badaniami przeprowadzonymi przez Instytut Badawczy Dróg i Mostów we Wrocławiu, Centralny Ośrodek Badawczo-Rozwojowy Przemysłu Betonów „CEBET” w Warszawie, Katedrę Technologii Betonu i Prefabrykacji Politechniki świętokrzyskiej oraz Generalną Dyrekcje Dróg Krajowych i Autostrad. Odrębnym segmentem działalności kopalni jest obróbka i sprzedaż marmuru. Pomimo różnorodnych trendów i zmian na rynku gospodarczym ma ustabilizowaną, satysfakcjonującą sytuację. Niemała w tym zasługa strategicznych odbiorców Morawicy reprezentujących różne gałęzie przemysłu, dzięki którym odnotowuje systematyczny wzrost produkcji. Wiąże się to również z modernizacją i uzbrajaniem zakładu w nowoczesne maszyny i urządzenia. Działania firmy ukierunkowane są na nowe, przyszłościowe rozwiązania technologiczne oraz podnoszenie kwalifikacji pracowników – w dużej mierze ludzi młodych, dobrze przygotowanych do wykonywanego zawodu. Przez ponad pół wieku funkcjonowania na rynku zdobyła ogromne doświadczenie i zaufanie klientów. Kopalnia Wapienia „Morawica” SA to gwarancja jakości, dobra świętokrzyska marka i dobry produkt Polski. W firmie został ustanowiony oraz funkcjonuje Zintegrowany System Zarządzania Jakością według normy ISO 9001:2008 oraz Zakładowej Kontroli Produkcji według normy EN-13043. Działalność firmy w pełni zaspokaja oczekiwania odbiorców i gwarantuje pełną satysfakcję dotychczasowym i przyszłym klientom.</p>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up" class="row row--md-row-reverse page-mine__two-cols">
                <div class="col-xs-12 col-sm-6">
                    <img class="img-responsive" src="/images/mine.png" alt="Kopalnia">
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="cms-content">
                        <p>Zakres działania firmy: - wydobywanie i przerób kamienia wapiennego,<br />
                            - sprzedaż kruszyw dla budownictwa, drogownictwa,<br />
                            - sprzedaż kamienia przemysłowego dla hutnictwa, cukrowni, energetyki,<br />
                            - transport drogowy i kolejowy - wydobycie, obróbka i montaż wyrobów marmurowych, <br />
                            - produkcja i sprzedaż nawozów.</p>
                        <p>Dzięki sukcesywnie wdrażanym inwestycjom zaspokaja oczekiwania swoich odbiorców.
                            Gwarantuje pełna satysfakcję dotychczasowym i przyszłym klientom.</p>
                    </div>
                </div>
            </div>
            <div class="row" data-aos="fade-right">
                <div class="col-xs-12">
                    <div class="page-mine__partners">
                        <div class="section-header section-header--center">Partnerzy biznesowi</div>
                        <div class="page-mine__partners-images">
                            <img class="page-mine__partner" src="/images/partner1.png" alt="">
                            <img class="page-mine__partner" src="/images/partner2.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('components.Gallery.Gallery')
    </div>
@endsection
