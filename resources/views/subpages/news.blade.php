@extends('wrappers.default', ['module' => 'news'])

@section('subpage')
    <div class="page-news">
        @include('components.PageHeader.PageHeader', ['title' => 'Tytuł pierwszej aktualności w dwóch liniach', 'hasTable' => false, 'narrow' => true])
        <div class="container container--narrow">
            <div class="row">
                <div class="col-xs-12">
                    <div class="cms-content page-news__content">
                        <h5>Zasady Bezpieczeństwa i Ochrony Zdrowia</h5>
                        <p>Kopalnia Wapienia „Morawica” to znany i ceniony nie tylko w kraju, ale również za granicą producent kruszyw, marmuru i wapna nawozowego. Kopalnia Wapienia „Morawica” wypracowała swoją renomę nie tylko przez dbałość o wysoką jakość swoich produktów, ale również troskę o bezpieczeństwo i zdrowie pracowników swoich jak również pracowników podmiotów obcych wykonujących usługi na terenie naszej firmy. Za swoją działalność Kopalnia Wapienia „Morawica” była wielokrotnie nagradzana oraz otrzymała liczne certyfikaty. Kopalnia trzykrotnie otrzymała certyfikat „Pracodawca Przyjazny Pracownikom”, który jest nadawany między innymi za wyróżnianie się w zakresie przestrzegania przepisów prawa pracy oraz przestrzeganie zasad bezpieczeństwa i higieny pracy. To świadczy o tym, że bezpieczeństwo i ochrona zdrowia naszych pracowników jest bardzo ważną częścią naszej działalności. W celu osiągania coraz wyższej kultury bezpieczeństwa kierujemy się następującymi zasadami:</p>
                        <ul>
                            <li>1. Pracownicy na każdym szczeblu zobowiązani są do dbania o przestrzeganie przepisów i zasad BHP. Wszyscy zobowiązani są do kreowania wysokiej kultury bezpieczeństwa wśród załogi górniczej i wykonawców.</li>
                            <li>2. Każdy pracownik czy wykonawca przed przystąpieniem do pracy zostaje przeszkolony i zapoznany z ryzykiem, czynnikami szkodliwymi na stanowisku pracy oraz instrukcjami BHP dla danych stanowisk pracy.</li>
                            <li>3. Osoby kierujące pracownikami w trakcie przydzielania zadań każdorazowo informują pracowników o zakresie ich obowiązków oraz udzielają instruktażu BHP związanego z wykonywaniem tych zadań.</li>
                            <li>4. Każdy pracownik i wykonawca zobowiązani są do stosowania środków ochrony indywidualnej i innych środków bezpieczeństwa niezbędnych do wykonania danej pracy w sposób bezpieczny i niezagrażający zdrowiu.</li>
                            <li>5. Każdy pracownik i wykonawca mają prawo przerwać pracę, gdy ta zagraża ich bezpieczeństwu lub innych osób przebywających w pobliżu.</li>
                            <li>6. Każdy pracownik i wykonawca mają obowiązek zgłaszania wszystkich wypadków i zagrożeń w celu ich zbadania i opracowania procedur eliminujących takie zagrożenia.</li>
                            <li>7. Wymieniamy maszyny i urządzenia górnicze na nowoczesne, które zapewniają wysoki poziom bezpieczeństwa i ochrony zdrowia pracowników.</li>
                        </ul>
                        <iframe width="100%" src="https://www.youtube.com/embed/WXv31OmnKqQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        @include('components.Gallery.Gallery')
    </div>
@endsection
