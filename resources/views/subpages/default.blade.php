@extends('wrappers.default', ['module' => 'default'])

@section('subpage')
    <div class="page-default">
        @include('components.PageHeader.PageHeader', ['title' => 'Ochrona środowiska', 'hasTable' => false, 'narrow' => true])
        <div class="container container--narrow">
            <div class="row">
                <div class="col-xs-12">
                    <div class="cms-content page-news__content" data-aos="fade-up">
                        <h5>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual.</h5>
                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure? On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness</p>
                        <ul>
                            <li>1. Pracownicy na każdym szczeblu zobowiązani są do dbania o przestrzeganie przepisów i zasad BHP. Wszyscy zobowiązani są do kreowania wysokiej kultury bezpieczeństwa wśród załogi górniczej i wykonawców.</li>
                            <li>2. Każdy pracownik czy wykonawca przed przystąpieniem do pracy zostaje przeszkolony i zapoznany z ryzykiem, czynnikami szkodliwymi na stanowisku pracy oraz instrukcjami BHP dla danych stanowisk pracy.</li>
                            <li>3. Osoby kierujące pracownikami w trakcie przydzielania zadań każdorazowo informują pracowników o zakresie ich obowiązków oraz udzielają instruktażu BHP związanego z wykonywaniem tych zadań.</li>
                            <li>4. Każdy pracownik i wykonawca zobowiązani są do stosowania środków ochrony indywidualnej i innych środków bezpieczeństwa niezbędnych do wykonania danej pracy w sposób bezpieczny i niezagrażający zdrowiu.</li>
                            <li>5. Każdy pracownik i wykonawca mają prawo przerwać pracę, gdy ta zagraża ich bezpieczeństwu lub innych osób przebywających w pobliżu.</li>
                            <li>6. Każdy pracownik i wykonawca mają obowiązek zgłaszania wszystkich wypadków i zagrożeń w celu ich zbadania i opracowania procedur eliminujących takie zagrożenia.</li>
                            <li>7. Wymieniamy maszyny i urządzenia górnicze na nowoczesne, które zapewniają wysoki poziom bezpieczeństwa i ochrony zdrowia pracowników.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
