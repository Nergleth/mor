@extends('wrappers.layout')

@section('main-content')
    @include('components.topMenu.topMenu')
    <div class="subpage-wrapper">
        @yield('subpage')
    </div>
    @include('components.Footer.Footer')
@endsection
