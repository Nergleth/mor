<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('components.core.header')
    <body>
        @include('components.ContactForm.ContactForm')
        <div id="preloader" class="loader"><img src="/images/icons/preloader.svg" alt="ładowanie"></div>
        @include('components.core.bodyScripts')
        @yield('main-content')
        @include('components.core.bodyEndScripts')
    </body>
</html>
