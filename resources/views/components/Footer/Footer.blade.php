<footer class="footer">
    <div class="container">
        <div class="logo-slider">
            <div class="logo-slider__title section-header section-header--center" data-aos="fade-left">Zaufali nam</div>
            <div class="logo-slider__wrapper owl-carousel">
                <div class="logo-slider__image-wrapper">
                    <img class="img-responsive logo-slider__image" src="/images/logos/1.png" alt="Slide"/>
                </div>
                <div class="logo-slider__image-wrapper">
                    <img class="img-responsive logo-slider__image" src="/images/logos/2.png" alt="Slide"/>
                </div>
                <div class="logo-slider__image-wrapper">
                    <img class="img-responsive logo-slider__image" src="/images/logos/3.png" alt="Slide"/>
                </div>
                <div class="logo-slider__image-wrapper">
                    <img class="img-responsive logo-slider__image" src="/images/logos/4.png" alt="Slide"/>
                </div>
            </div>
        </div>
    </div>
    <div class="eu" data-aos="fade-in">
        <img class="img-responsive eu__image" src="/images/rpo.png" alt="RPO"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-6" data-aos="fade-up">
                <h5 class="footer__title">KW Morawica</h5>
                <div class="cms-content footer__short-desc-content"><p>Kopalnia Wapienia „Morawica”
                        SA położona jest w województwie świętokrzyskim, 13 km od Kielc na terenie tzw.
                        Białego Zagłębia. Funkcjonuje już od ponad 40 lat, chociaż kamień
                        wapienny wydobywa się na tych terenach od ok. 150 lat.</p>
                </div>
                <div class="footer__short-desc-socials" data-aos="fade-right">
                    <span>Odwiedź nas: </span>
                    <a href="/"><img class="img-responsive logo-slider__image" src="/images/icons/youtube.svg" alt="Slide"/></a>
                    <a href="/"><img class="img-responsive logo-slider__image" src="/images/icons/fb.svg" alt="Slide"/></a>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-md-6" data-aos="fade-up">
                        <h5 class="footer__title">Aktualności</h5>
                        <a href="/news" class="footer__news">
                            <img class="img-responsive footer__news-image" src="/images/news-1.png" alt="News"/>
                            <p class="footer__news-title">Nowe produkty w ofercie handlowej Kopalni Wapienia "Morawica" S.A.</p>
                        </a>
                        <a href="/news" class="footer__news">
                            <img class="img-responsive footer__news-image" src="/images/news-2.png" alt="News"/>
                            <p class="footer__news-title">Nasze Dobre Świętokrzyskie</p>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h5 class="footer__title">Nawigacja</h5>
                        <ul class="footer__menu" data-aos="fade-right">
                            <li class="footer__menu-item"><a href="/" class="footer__menu-link" title="menu item">Oferta</a></li>
                            <li class="footer__menu-item"><a href="/" class="footer__menu-link" title="menu item">O firmie</a></li>
                            <li class="footer__menu-item"><a href="/" class="footer__menu-link" title="menu item">Zrównoważony rozwój</a></li>
                            <li class="footer__menu-item"><a href="/" class="footer__menu-link" title="menu item">Kopalnia</a></li>
                            <li class="footer__menu-item"><a href="/" class="footer__menu-link" title="menu item">Kariera</a></li>
                            <li class="footer__menu-item"><a href="/" class="footer__menu-link" title="menu item">Kontakt</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="footer__copy">
                    <span class="footer__copy-left">Copyright {{date("Y")}} by KW Morawica. Wszelkie prawa zastrzeżone.</span>
                    <span class="footer__copy-right">Realizacja: <a href="https://massinternet.pl">Mass Internet</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
