<div data-aos="fade-in" class="page-header">
    <div class="container @if (isset($narrow) || (isset($narrow) && $narrow === true)) container--narrow @endif">
        <div class="page-header__title">{{$title}}</div>
        @if ($hasTable)
            <div class="page-header__table">
                <div class="page-header__table-label">Kruszywa łamane drogowe</div>
                <div class="page-header__table-wrapper">
                    <div class="page-header__table-elements">
                        @for ($i = 1; $i<16; $i++)
                            <div class="page-header__table-element">4-120mm</div>
                        @endfor
                    </div>
                    <a href="" class="page-header__table-btn">Granulacje na zamówienie</a>
                </div>
            </div>
        @endif
        <img alt="Kruszywa" class="page-header__image" src="/images/kruszywa.png"/>
    </div>
</div>
