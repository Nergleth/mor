<div class="slider">
    <div class="hidden-xs fullScreenSlider owl-carousel">
        <div class="slider__slide">
            <div class="slider__content">
                <span class="slider__first-line-text">Eksperci w wydobyciu kamienia</span>
                <span class="slider__second-line-text">Mamy ponad 55 lat doświadzczenia!</span>
                <a href="http://google.pl" class="yellow-btn"><span>Poznaj naszą firmę</span></a>
            </div>
            <img class="img-responsive slider__img" src="/images/slide.png" alt="Slide"/>
        </div>
        <div class="slider__slide">
            <div class="slider__content">
                <span class="slider__first-line-text">Eksperci w wydobyciu kamienia</span>
                <span class="slider__second-line-text">Drugi slajd!</span>
                <a href="http://google.pl" class="yellow-btn"><span>Poznaj naszą firmę</span></a>
            </div>
            <img class="img-responsive slider__img" src="/images/slide.png" alt="Slide"/>
        </div>
    </div>
    <div class="slider__info" id="js-slider-info"></div>
    <div id="carousel-custom-dots" class="owl-dots hidden-xs">
        <span class="owl-dot"><span></span></span>
        <span class="owl-dot"><span></span></span>
    </div>
</div>
