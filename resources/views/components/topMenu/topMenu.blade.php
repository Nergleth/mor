<div class="top-menu" data-aos="fade-up">
    <a href="/" class="top-menu__logo">
        <img class="top-menu__logo-image" src="/images/logo.png" alt="">
    </a>
    <div class="top-menu__right">
        <div class="top-menu__top-info">
            <span class="top-menu__top-info-text">Kopalnia wapienia "Morawica S.A."</span>
            <span class="top-menu__top-info-text">ul. Górnicza 42, 26-026 Morawica k/Kielc</span>
        </div>
        <div class="top-menu__navigation" id="js-top-menu">
            <div class="top-menu__toggler top-menu__toggler--close js-toggle-menu"></div>
            <ul class="top-menu__navigation-list">
                <li class="top-menu__navigation-elem has-submenu">
                    <a class="top-menu__navigation-link">
                        <span>Oferta</span>
                    </a>
                    <ul class="top-menu__submenu">
                        <li class="top-menu__submenu-item"><a href="/product" class="top-menu__submenu-link">Kruszywa drogowe</a></li>
                        <li class="top-menu__submenu-item"><a href="/product" class="top-menu__submenu-link">Marmur Morawica</a></li>
                        <li class="top-menu__submenu-item"><a href="/product" class="top-menu__submenu-link">Nawozy rolnicze</a></li>
                        <li class="top-menu__submenu-item"><a href="/product" class="top-menu__submenu-link">Kamień przemysłowy</a></li>
                    </ul>
                </li>
                <li class="top-menu__navigation-elem has-submenu"><a class="top-menu__navigation-link" ><span>O firmie</span></a>
                    <ul class="top-menu__submenu">
                        <li class="top-menu__submenu-item"><a href="/history" class="top-menu__submenu-link">Historia firmy</a></li>
                        <li class="top-menu__submenu-item"><a href="/board" class="top-menu__submenu-link">Zarząd</a></li>
                        <li class="top-menu__submenu-item"><a href="/qualitypolicy" class="top-menu__submenu-link">Polityka jakości</a></li>
                        <li class="top-menu__submenu-item"><a href="/subsidies" class="top-menu__submenu-link">Dotacje</a></li>
                        <li class="top-menu__submenu-item"><a href="/awards" class="top-menu__submenu-link">Nagrody i wyróżnienia</a></li>
                    </ul>
                </li>
                <li class="top-menu__navigation-elem has-submenu"><a class="top-menu__navigation-link"><span>Zrównoważony rozwój</span></a>
                    <ul class="top-menu__submenu">
                        <li class="top-menu__submenu-item"><a href="/default" class="top-menu__submenu-link">Ochrona środowiska</a></li>
                        <li class="top-menu__submenu-item"><a href="/default" class="top-menu__submenu-link">Odnawialne źródła energii</a></li>
                        <li class="top-menu__submenu-item"><a href="/default" class="top-menu__submenu-link">Bezpieczeńśtwo pracy</a></li>
                    </ul>
                </li>
                <li class="top-menu__navigation-elem"><a class="top-menu__navigation-link" href="/mine"><span>Kopalnia</span></a></li>
                <li class="top-menu__navigation-elem"><a class="top-menu__navigation-link" href="/career"><span>Praca</span></a></li>
                <li class="top-menu__navigation-elem"><a class="top-menu__navigation-link" href="/contact"><span>Kontakt</span></a></li>
            </ul>
        </div>
        <a href="tel:41 36 70 211" class="top-menu__phone-number"><span>41 36 70 211</span></a>
        <div class="top-menu__email js-form-toggler" id=""><span>Napisz do nas</span></div>
        <div class="top-menu__toggler js-toggle-menu"></div>
    </div>
</div>
