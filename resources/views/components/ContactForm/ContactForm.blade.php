<form method="post" name="contact-form" class="contact-form" id="js-contact-form">
    <div class="contact-form__close js-form-toggler"></div>
    <div class="contact-form__header">Napisz wiadomość</div>
    <input name="" type="text" class="contact-form__input" placeholder="Temat wiadomości" />
    <input name="" type="text" class="contact-form__input" placeholder="Imię i nazwisko" />
    <input name="" type="text" class="contact-form__input" placeholder="Telefon" />
    <input name="" type="email" class="contact-form__input" placeholder="Adres Email" />
    <textarea rows="4" cols="50" name="" class="contact-form__input contact-form__input--textarea" placeholder="Treść wiadomości"></textarea>
    <div class="contact-form__btn-wrapper">
        <div class="contact-form__btn yellow-btn">
            <span>Wyślij</span>
        </div>
    </div>
</form>
