<div class="subsidy" data-aos="fade-up">
    <div class="subsidy__title">{{$title}}</div>
    <div class="subsidy__content cms-content">
        <img src="/images/subsidies3.png" alt="">
        <p>Dotyczy rozstrzygnięcia zapytania ofertowego nr 1 z dnia 23-02-2018 r. </p>
        <p>Zapytanie ofertowe zostało przeprowadzone w związku z realizacją projektu: „Zakup mobilnego dwupokła
            dowego przesiewacza celem wdrożenia w Kopalnia Wapienia „Morawica" S.A. innowacyjności produktowej
            oraz procesowej z zakresu produkcji kruszyw.” w ramach Regionalnego Programu Operacyjnego Województwa
            Świętokrzyskiego na lata 2014-2020. </p>
        <p>Oś priorytetowa 2 Konkurencyjna Gospodarka Działanie 2.5 Wsparcie inwestycyjne sektora MŚP W ramach prowadzonego postępowania wyłoniony został dostawca, którym jest: </p>
        <p>Powers Maszyny sp. z o.o. ul. Poznańska 99, Czapury, 61-160 Poznań Cena netto: 949 000,00 PLN Data wpływu oferty: 26.03.2018</p>
    </div>
    <div class="subsidy__files">
        <a class="subsidy__file subsidy__file--pdf" href="#" title="">Ogłoszenie</a>
        <a class="subsidy__file subsidy__file--doc" href="#" title="">Ogłoszenie</a>
    </div>
    <div class="subsidy__footer">
        <div class="cms-content">
            Ogłoszenie zostało zamieszczone także na stronie:
            <a href="https://bazakonkurencyjnosci.funduszeeuropejskie.gov.pl/publication/view/1091437">https://bazakonkurencyjnosci.funduszeeuropejskie.gov.pl/publication/view/1091437</a></div>
    </div>
</div>
