<title>Kopalnia Morawica</title>
<meta name="og:title" content=""/>
<meta name="title" property="og:title" content=""/>
<meta name="og:type" content="website"/>
<meta name="og:image" content=""/>
<meta name="image" property="og:image" content=""/>
<meta name="og:site_name" content=""/>
<meta name="og:description" content=""/>
<meta name="description" property="og:description" content="">
<meta name="keywords" content="">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,900&display=swap&subset=latin-ext" rel="stylesheet">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="index, follow">
<link rel="canonical" href="">

<meta name="format-detection" content="telephone=no">

<meta name="csrf-token" content="{{ csrf_token() }}">
<link type="text/css" href="assets/{{$module}}.css" rel="stylesheet">

<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
<link rel="manifest" href="/images/favicon/site.webmanifest">
<link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/images/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-config" content="/images/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<style>
    .loader{width:100%; height:100%; background:#fff; position:fixed; top:0; left:0; transition:all 0.3s ease-in-out; opacity:1; visibility:visible; z-index:300;}
    .loader.closed{opacity:0; visibility:hidden;z-index:300;}
    .loader img{top:50%;left:50%;transform:translate(-50%, -50%); position:absolute;}
</style>
