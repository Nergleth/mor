<div class="gallery">
    <div class="row no-padding">
        @for ($i = 1; $i < 13; $i++)
            <div class="col-xs-6 col-sm-3 col-lg-2 no-padding" data-aos="fade-up">
                <a class="colorbox gallery__item" rel="gallery" href="/images/gallery-item.jpg">
                    <img class="gallery__image img-responsive" src="/images/gallery-item.jpg" alt="Gallery-item">
                </a>
            </div>
        @endfor
    </div>
</div>
