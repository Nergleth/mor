<div class="corner-header">
    <div data-aos="fade-right" class="container"><h1 class="corner-header__title">{{$title}}</h1></div>
</div>
