<div class="download">
    <div class="container">
        <div data-aos="fade-up" class="download__header @if(isset($centered)) download__header--centered @endif">Materiały do pobrania</div>
        <div data-aos="fade-down" class="row @if (isset($centered)) row--xs-justify-content-center @endif">
            <div class="col-xs-12 col-sm-4 col-lg-8col">
                <a href="/" class="download__box">
                    <span class="download__icon">
                        <img src="/images/icons/download.svg" alt="Pobieranie">
                    </span>
                    <span class="download__title">Pliki do pobrania</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-8col">
                <a href="/" class="download__box">
                    <span class="download__icon">
                        <img src="/images/icons/download.svg" alt="Pobieranie">
                    </span>
                    <span class="download__title">Pliki do pobrania</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-8col">
                <a href="/" class="download__box">
                    <span class="download__icon">
                        <img src="/images/icons/download.svg" alt="Pobieranie">
                    </span>
                    <span class="download__title">Pliki do pobrania</span>
                </a>
            </div>
        </div>
    </div>
</div>
