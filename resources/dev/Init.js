import AOS from 'aos';
import './styles/main.scss';

import topMenu from './components/topMenu/topMenu';
import Footer from './components/Footer/Footer';
import ContactForm from './components/ContactForm/ContactForm';

const fadeLoader = () =>
    setTimeout(() => {
        document.getElementById('preloader').classList.add('closed');
        AOS.init({
            startEvent: 'DOMContentLoaded',
            debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
            throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
            anchorPlacement: 'top-bottom',
        });
    }, 500);

const Init = module => {
    const init = () => {
        fadeLoader();
        topMenu();
        Footer();
        ContactForm();
        module();
    };

    window.addEventListener('load', init);
};

export default Init;
