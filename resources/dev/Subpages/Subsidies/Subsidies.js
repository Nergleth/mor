import Sticky from 'sticky-js';

import Init from '../../Init';
import './Subsidies.scss';
import CornerHeader from '../../components/CornerHeader/CornerHeader';
import Subsidy from '../../components/Subsidy/Subsidy';

const Subsidies = () => {
    CornerHeader();
    Subsidy();

    const enableStickyMenu = () => {
        const stickyMenu = new Sticky('.js-sticky');
    };

    enableStickyMenu();
};

Init(Subsidies);
