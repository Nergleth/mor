import Init from '../../Init';
import './Career.scss';
import PageHeader from '../../components/PageHeader/PageHeader';

const Career = () => {
    PageHeader();

    const handleJobOffer = () => {
        const offers = [...document.getElementsByClassName('js-job-offer')];

        const offerToggleHandler = offer => {
            const offerCloser = offer.getElementsByClassName('js-offer-closer')[0];
            const offerDetails = offer.getElementsByClassName('js-offer-details')[0];
            const offerBtn = offer.getElementsByClassName('js-offer-opener')[0];

            const toggleOffer = () =>
                [offerBtn, offerCloser, offerDetails].map(toggler =>
                    toggler.classList.toggle('hidden'));

            [offerBtn, offerCloser].map(toggler =>
                toggler.addEventListener('click', toggleOffer)
            );
        };

        offers.map(offer => offerToggleHandler(offer));
    };

    handleJobOffer();
};

Init(Career);
