import Init from '../../Init';
import './Product.scss';
import Download from '../../components/Download/Download';
import Gallery from '../../components/Gallery/Gallery';
import PageHeader from '../../components/PageHeader/PageHeader';

const Product = () => {
    Download();
    Gallery();
    PageHeader();
};

Init(Product);
