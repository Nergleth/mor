import Init from '../../Init';
import './Mine.scss';
import Gallery from '../../components/Gallery/Gallery';
import PageHeader from '../../components/PageHeader/PageHeader';

const Mine = () => {
    Gallery();
    PageHeader();
};

Init(Mine);
