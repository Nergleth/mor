import Init from '../../Init';
import './Contact.scss';
import CornerHeader from '../../components/CornerHeader/CornerHeader';
import Download from '../../components/Download/Download';

const Contact = () => {
    CornerHeader();
    Download();

    const handleDepartmentChoose = () => {
        const depContents = [...document.getElementsByClassName('js-dep-content')];
        const dep = document.getElementById('js-dep');
        const choose = dep.getElementsByTagName('div')[0];
        const list = dep.getElementsByTagName('ul')[0];
        const listElems = [...dep.getElementsByTagName('li')];
        document.addEventListener('click', () =>
            list.classList.remove('active')
        );

        const toggleList = e => {
            e.stopPropagation();
            list.classList.toggle('active');
        };

        const changeDep = e => {
            if (!e.currentTarget.classList.contains('active')) {
                list.classList.remove('active');
                choose.innerText = e.currentTarget.innerText;
                listElems.map(elem =>
                    e.currentTarget === elem
                        ? elem.classList.add('active')
                        : elem.classList.remove('active')
                );
                e.currentTarget.classList.add('active');

                depContents.map(content =>
                    content.classList.contains(`dep-${e.currentTarget.dataset.id}`)
                        ? content.classList.add('active')
                        : content.classList.remove('active')
                );
            }
        };

        choose.addEventListener('click', toggleList);

        listElems.map(elem => elem.addEventListener('click', changeDep));
    };

    handleDepartmentChoose();
};

Init(Contact);
