import Init from '../../Init';
import './News.scss';
import Gallery from '../../components/Gallery/Gallery';
import PageHeader from '../../components/PageHeader/PageHeader';

const News = () => {
    Gallery();
    PageHeader();
};

Init(News);
