import './ContactForm.scss';

export default () => {
    const togglers = [...document.getElementsByClassName('js-form-toggler')];
    const form = document.getElementById('js-contact-form');

    const toggleContactForm = e => {
        form.classList.toggle('active');
    };

    togglers.map(toggler =>
        toggler.addEventListener('click', toggleContactForm)
    );
};
