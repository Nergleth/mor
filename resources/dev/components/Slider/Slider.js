import 'owl.carousel.es6';

import './Slider.scss';

const Slider = () => {
    const slider = document.getElementsByClassName('fullScreenSlider')[0];
    const sliderInfo = document.getElementById('js-slider-info');
    let dots = null;
    const autoplay = 5000;

    const prev = "<i class='fa fa-angle-left'></i>";
    const next = "<i class='fa fa-angle-right'></i>";

    const changeSlider = event => {
        let w = 0;
        sliderInfo.innerText = `${event.page.index + 1} / ${event.page.count}`;

        let progressbar = document.querySelector('#active-progress');

        if (progressbar) {
            progressbar.remove();
        }

        if (dots) {
            const activeDot = dots.getElementsByClassName('active')[0];
            const progress = document.createElement('div');
            progress.setAttribute('id', 'active-progress');
            activeDot.appendChild(progress);
            progressbar = document.querySelector('#active-progress');

            const styles = window.getComputedStyle(progressbar);

            const incr = () => {
                if (w >= 100) {
                    clearInterval(interval);
                } else {
                    w += 25;
                }
                progressbar.style.width = `${w}%`;
            };

            const interval = setInterval(incr, 1000);
        }
    };

    $(document).ready(function () {
        const setDots = event => {
            dots = document.getElementsByClassName('owl-dots')[0];
            changeSlider(event);
        };

        const carousel = $('.fullScreenSlider').owlCarousel({
            loop: true,
            margin: 0,
            items: 1,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            autoplayTimeout: autoplay,
            mouseDrag: true,
            // autoplayHoverPause: true,
            autoplay: true,
            dotsContainer: '#carousel-custom-dots',
            responsiveClass: true,
            navText: [prev, next],
            onChanged: event => changeSlider(event),
            onInitialized: event => setDots(event),
            dots: true,
            nav: false,
        });
    });
};

export default Slider;
