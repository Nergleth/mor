import './Gallery.scss';
import 'jquery-colorbox';

export default () => {
    $('.colorbox').colorbox({
        rel: 'gallery',
        maxWidth: '95%',
        maxHeight: '95%',
        scrolling: false,
        fixed: true,
        title: '',
    });
};
