<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SampleController extends Controller
{
    public function ShowSubpage($name = '') {
        if ($name === '' || $name === '/') {
            return view('subpages.homepage');
        }

        return view('subpages.'.strtolower($name));
    }
}
